#include <stdio.h>

#ifdef __MINGW32__
#include <fcntl.h>
#include <io.h>
#endif

int
main(void)
{
#ifdef __MINGW32__
	// Set the stdout translation mode to binary to prevent printf() from
	// changing '\n' to '\r\n', as this trips up tap-driver.sh.
	fflush(stdout);
	_setmode(1, _O_BINARY);
#endif
	printf("1..1\n");
#ifdef __MINGW32__
	// Revert back to text mode, since the problem only occurs when parsing
	// the test plan.
	fflush(stdout);
	_setmode(1, _O_TEXT);
#endif

	printf("ok 1\n");

	return 0;
}

