#ifndef HELLO_INTERN_HELLO_H
#define HELLO_INTERN_HELLO_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#define HELLO_INTERN	1
#include <hello/hello.h>

#ifndef HELLO_EXPORT
#if defined(_WIN32) && (defined(_MSC_VER) || defined(__declspec)) \
		&& defined(DLL_EXPORT)

#define HELLO_EXPORT	__declspec(dllexport)
#else
#define HELLO_EXPORT
#endif
#endif

#endif

