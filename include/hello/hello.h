#ifndef HELLO_HELLO_H
#define HELLO_HELLO_H

#ifndef HELLO_EXTERN
#if defined(_WIN32) && (defined(_MSC_VER) || defined(__declspec)) \
		&& defined(DLL_EXPORT)
#ifdef HELLO_INTERN
#define HELLO_EXTERN	extern __declspec(dllexport)
#else
#define HELLO_EXTERN	extern __declspec(dllimport)
#endif
#else
#define HELLO_EXTERN	extern
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

HELLO_EXTERN void hello(void);

#ifdef __cplusplus
}
#endif

#endif

